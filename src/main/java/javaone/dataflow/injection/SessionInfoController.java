package javaone.dataflow.injection;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

public class SessionInfoController {

    @RequestMapping(value="/displayProperty", method= RequestMethod.GET)
    public @ResponseBody String displayProperty(@RequestParam String property,HttpServletRequest req) {

        SessionIdentifyDTO identifyDTO = (SessionIdentifyDTO) req.getSession().getAttribute("identity");

        return SessionUtil.getUserProperty(identifyDTO,property);
    }
}
