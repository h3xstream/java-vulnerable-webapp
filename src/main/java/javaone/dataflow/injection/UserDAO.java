package javaone.dataflow.injection;

import javaone.util.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import javax.inject.Qualifier;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Repository
public class UserDAO {

    protected EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    public UserEntity getUserByUsername(String username) {
        TypedQuery<UserEntity> q = entityManager.createQuery(
                String.format("select * from Users where name = '%s'", username),
                UserEntity.class);

        return q.getSingleResult();
    }
}
