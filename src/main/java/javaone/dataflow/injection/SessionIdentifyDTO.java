package javaone.dataflow.injection;

public class SessionIdentifyDTO {

    public final String firstName;
    public final String lastName;

    public SessionIdentifyDTO(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
