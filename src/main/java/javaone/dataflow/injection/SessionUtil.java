package javaone.dataflow.injection;


import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

public class SessionUtil {


    public static String getUserProperty(SessionIdentifyDTO person,String property) {

        ExpressionParser parser = new SpelExpressionParser();

        StandardEvaluationContext userContext = new StandardEvaluationContext(person);
        //Reading the property
        Expression expr = parser.parseExpression(property);
        return expr.getValue(userContext, String.class);
    }






    public static void main(String[] args) {

        SessionIdentifyDTO demoDto = new SessionIdentifyDTO("Benoit", "Guerette");

        //Expected use case..
        String res = getUserProperty(demoDto, "firstName");
        System.out.println("exp2="+res);
        //Malicious use case..
        res = getUserProperty(demoDto, "T(java.lang.Runtime).getRuntime().exec('calc.exe')"); //start the calc virus :)
        System.out.println("exp2="+res);
    }
}
