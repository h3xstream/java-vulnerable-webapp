package javaone.dataflow.pathtraversal;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class ResourceController {

    @Inject
    MyControllerFacade controller;


    @GET
    @Path("/images_thumbs/{image}")
    @Produces("images/*")
    public Response getImageThumbnail(@PathParam("image") String image) {
        byte[] content = controller.getImageThumbnail(image,false);

        return Response.ok().entity(content).build();
    }
}
