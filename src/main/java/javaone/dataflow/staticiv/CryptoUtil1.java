package javaone.dataflow.staticiv;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Base64;

public class CryptoUtil1 {

    String key = "This is the super secret key 123";

    byte[] ivBytes = {
            0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x13, 0x37, 0x13, 0x37, 0x13, 0x37
    };


    String plainText;
    byte[] cipherData;
    String base64Text;
    String cipherText;


    /*
    The function that handles the aes256 encryption.
    ivBytes: Initialization vector used by the encryption function
    keyBytes: Key used as input by the encryption function
    textBytes: Plaintext input to the encryption function
    */
    public static byte[] aes256encrypt(byte[] iv, byte[] keyBytes, byte[] textBytes)
            throws java.io.UnsupportedEncodingException,
            NoSuchAlgorithmException,
            NoSuchPaddingException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException {

        AlgorithmParameterSpec ivSpec = new IvParameterSpec(iv);
        SecretKeySpec newKey = new SecretKeySpec(keyBytes, "AES");
        Cipher cipher = null;
        cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, newKey, ivSpec);
        return cipher.doFinal(textBytes);
    }

    /*
    The function that handles the aes256 decryption.
    ivBytes: Initialization vector used by the decryption function
    keyBytes: Key used as input by the decryption function
    textBytes: Ciphertext input to the decryption function
    */
    public static byte[] aes256decrypt(byte[] ivBytes, byte[] keyBytes, byte[] textBytes)
            throws java.io.UnsupportedEncodingException,
            NoSuchAlgorithmException,
            NoSuchPaddingException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException {

        AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
        SecretKeySpec newKey = new SecretKeySpec(keyBytes, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, newKey, ivSpec);
        return cipher.doFinal(textBytes);

    }

    /*
    The function that uses the aes256 decryption function
    theString: Ciphertext input to the decryption function
    plainText: Plaintext output of the encryption operation
    */
    public String aesDeccryptedString(String theString) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

        byte[] keyBytes = key.getBytes("UTF-8");
        cipherData = CryptoUtil1.aes256decrypt(ivBytes, keyBytes, Base64.getDecoder().decode(theString.getBytes("UTF-8")));
        plainText = new String(cipherData, "UTF-8");
        return plainText;
    }

    /*
    The function that uses the aes256 encryption function
    theString: Plaintext input to the encryption function
    cipherText: Ciphertext output of the encryption operation
    */
    public String aesEncryptedString(String theString) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

        byte[] keyBytes = key.getBytes("UTF-8");
        plainText = theString;
        cipherData = CryptoUtil1.aes256encrypt(ivBytes, keyBytes, plainText.getBytes("UTF-8"));
        cipherText = new String(Base64.getEncoder().encode(cipherData),"UTF-8");
        return cipherText;
    }
}
