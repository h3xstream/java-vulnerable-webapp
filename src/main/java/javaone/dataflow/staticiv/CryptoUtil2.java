package javaone.dataflow.staticiv;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Base64;

public class CryptoUtil2 {



    /*
    The function that handles the aes256 encryption.
    ivBytes: Initialization vector used by the encryption function
    keyBytes: Key used as input by the encryption function
    textBytes: Plaintext input to the encryption function
    */
    public static byte[] aes256encrypt(byte[] iv, byte[] keyBytes, byte[] textBytes)
            throws java.io.UnsupportedEncodingException,
            NoSuchAlgorithmException,
            NoSuchPaddingException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException {

        AlgorithmParameterSpec ivSpec = new IvParameterSpec(iv);
        SecretKeySpec newKey = new SecretKeySpec(keyBytes, "AES");
        Cipher cipher = null;
        cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, newKey, ivSpec);
        return cipher.doFinal(textBytes);
    }

    /*
    The function that uses the aes256 encryption function
    theString: Plaintext input to the encryption function
    cipherText: Ciphertext output of the encryption operation
    */
    public String aesEncryptedString(String plainText, byte[] keyBytes) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

        byte[] iv = new byte[16];
        new SecureRandom().nextBytes(iv);

        byte[] cipherText = CryptoUtil2.aes256encrypt(iv, keyBytes, plainText.getBytes("UTF-8"));
        byte[] fullCipherText = mergeByteArray(iv,cipherText);

        return new String(Base64.getEncoder().encode(fullCipherText),"UTF-8");
    }

    private static byte[] mergeByteArray(byte[] a, byte[] b) {
        byte[] c = new byte[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }
}
