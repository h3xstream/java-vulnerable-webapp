package javaone.dataflow.xml;

import javaone.dataflow.xml.DataImportXml;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TestXmlParsing {

    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
        DataImportXml dataImport = new DataImportXml();

        dataImport.importConfiguration(readFile("C:\\Code\\workspace-java\\java-vulnerable-webapp\\data1.xml"));
    }

    private static String readFile(String file) throws FileNotFoundException {
        return (new java.util.Scanner(new FileInputStream(file),"UTF-8")).useDelimiter("\\A").next();
    }

}
