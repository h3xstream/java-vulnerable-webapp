package javaone.dataflow.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class DataImportXml {

    private void receiveXMLStream(final InputStream inStream, final DefaultHandler defHandler)
            throws ParserConfigurationException, SAXException, IOException {
        // ...
        SAXParserFactory spf = SAXParserFactory.newInstance();
        final SAXParser saxParser = spf.newSAXParser();
        saxParser.parse(inStream, defHandler);
    }

    public void importConfiguration(String xmlString) throws IOException, SAXException, ParserConfigurationException {
        InputStream is = new ByteArrayInputStream(xmlString.getBytes());
        importConfiguration(is);
    }

    public void importConfiguration(InputStream xmlInputStream) throws IOException, SAXException, ParserConfigurationException {

        receiveXMLStream(xmlInputStream, new DefaultHandler() {
            StringBuilder content = new StringBuilder();

            @Override
            public void startElement (String uri, String localName,
                                      String qName, Attributes attributes)
                    throws SAXException {
                content.setLength(0);
                System.out.println("Node: "+qName);
            }

            @Override
            public void endElement(String uri, String localName, String qName)
                    throws SAXException {
                if(content.toString().trim().length() > 1)
                    System.out.println(content.toString()); //Show the node content
                content.setLength(0);
            }

            @Override
            public void characters(char ch[], int start, int length)
                    throws SAXException {
                this.content.append(ch, start, length);
            }
        });
    }
}


//spf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
