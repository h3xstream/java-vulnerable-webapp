package javaone.context.xml;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Logger;

@Controller
public class ConfigurationUploadController {

    private static final Logger log = Logger.getLogger(ConfigurationUploadController.class.getName());

    @RequestMapping(value="/upload", method=RequestMethod.GET)
    public @ResponseBody String provideUploadInfo() {
        return "This is the provisoning api to upload configuration.";
    }

    @RequestMapping(value="/upload", method=RequestMethod.POST)
    public @ResponseBody String handleConfigUpdate(@RequestParam("name") String configName) throws FileNotFoundException {
        log.info("Loading "+configName);
        File configFile = new File("/opt/superapp/configurations/"+configName);
        FileInputStream configIn = new FileInputStream(configFile);

        if (!configFile.exists()) {
            try {
                new DataImportXml().importConfiguration(configIn);

                return "You successfully uploaded " + configName + "!";
            } catch (Exception e) {
                return "You failed to upload " + configName + " => " + e.getMessage();
            }
        } else {
            return "File not found.";
        }
    }

}