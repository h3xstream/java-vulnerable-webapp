package javaone.context.header;

import javaone.util.MonitoringApi;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/monitoring")
public class AdminController {

    @RequestMapping(value="/usersSessions", method= RequestMethod.GET)
    public @ResponseBody String showCurrentUsers(HttpServletRequest request) {

        //The user sessions should only be access from administrator connected with SSH
        verifyLocalAccess(request);

        return MonitoringApi.showSessions();
    }

    private void verifyLocalAccess(HttpServletRequest request) {
        if(!"localhost".equals(request.getHeader("Host"))) {
            throw new UnauthorizedException();
        }
    }
}
